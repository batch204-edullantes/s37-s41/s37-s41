const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");


//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}






//Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})


	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			// bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>);

			if(isPasswordCorrect) {
				return {acces: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
} 

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		if(result == null) {
			return false;
		} else {
			result.password = "";
			return result;
		}
	});
}

//Enroll user to a class
module.exports.enroll = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		let isUserUpdated = await User.findById(data.userId).then( user => {

			user.enrollments.push({courseId: data.courseId});

			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true
				
				}
		
			});
		});


		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

				course.enrollees.push({userId: data.userId})

				return course.save().then((course, error) => {

					if(error) {
						return false

					} else {
						
						return true
					}
				});

		});



		if(isUserUpdated && isCourseUpdated) {

			return true

		} else {

			return false
		}



	}


}
