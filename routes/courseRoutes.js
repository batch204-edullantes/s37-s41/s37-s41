const express = require('express');
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course

router.post('/',auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("User is not an Admin!");
	}
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then((resultFromController) => {
		res.send(resultFromController);
	});
});

// Route for retrieving all the Active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for Updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;